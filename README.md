# GlyTouCan Format Menu Web Component 

This is a web component that displays the menu of options to choose the format for images to be displayed.

The style, format and notation parameters can be configured with attributes.

A [dropdown list](https://www.webcomponents.org/element/@polymer/paper-dropdown-menu) will be used to pick a format.  The API server outputs binary image data, so it can be output directly as image source.
