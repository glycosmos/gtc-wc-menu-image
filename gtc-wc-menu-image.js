"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const lit_element_1 = require("lit-element");
require("@polymer/paper-dropdown-menu/paper-dropdown-menu.js");
require("@polymer/paper-item/paper-item.js");
require("@polymer/paper-listbox/paper-listbox.js");
require("web-animations-js/web-animations-next.min.js");
let GtcWcMenuImage = class GtcWcMenuImage extends lit_element_1.LitElement {
    constructor() {
        super(...arguments);
        this.imagestyle = "extended";
        this.format = "png";
        this.notation = "snfg";
    }
    render() {
        return lit_element_1.html `
<style>
</style>
  <div>

<paper-dropdown-menu label="Style">
  <paper-listbox slot="dropdown-content" selected="${this.imagestyle}" attr-for-selected="value" @iron-select="${this.updateStyle}">
    <paper-item value="extended">extended</paper-item>
  </paper-listbox>
</paper-dropdown-menu>

<paper-dropdown-menu label="Format" >
<paper-listbox slot="dropdown-content" selected="${this.format}" attr-for-selected="value" @iron-select="${this.updateFormat}">
    <paper-item value="png">png</paper-item>
    <paper-item value="svg">svg</paper-item>
  </paper-listbox>
</paper-dropdown-menu>

<paper-dropdown-menu label="Notation">
  <paper-listbox slot="dropdown-content" selected="${this.notation}" attr-for-selected="value" @iron-select="${this.updateNotation}">
    <paper-item value="snfg">snfg</paper-item>
    <paper-item value="cfg">cfg</paper-item>
    <paper-item value="cfgbw">cfgbw</paper-item>
    <paper-item value="cfg-uoxf">cfg-uoxf</paper-item>
    <paper-item value="uoxf">uoxf</paper-item>
    <paper-item value="uoxf-color">uoxf-color</paper-item>
    <paper-item value="iupac">iupac</paper-item>
  </paper-listbox>
</paper-dropdown-menu>
  </div>
`;
    }
    updateStyle(e) {
        this.imagestyle = e.detail.item.innerText;
        let event = new CustomEvent('style-update-event', {
            detail: {
                message: 'style changed to' + this.imagestyle,
                value: this.imagestyle
            }
        });
        this.dispatchEvent(event);
    }
    updateFormat(e) {
        this.format = e.detail.item.innerText;
        let event = new CustomEvent('format-update-event', {
            detail: {
                message: 'format changed to' + this.format,
                value: this.format
            }
        });
        this.dispatchEvent(event);
    }
    updateNotation(e) {
        this.notation = e.detail.item.innerText;
        let event = new CustomEvent('notation-update-event', {
            detail: {
                message: 'notation changed to' + this.notation,
                value: this.notation
            }
        });
        this.dispatchEvent(event);
    }
};
__decorate([
    lit_element_1.property()
], GtcWcMenuImage.prototype, "imagestyle", void 0);
__decorate([
    lit_element_1.property()
], GtcWcMenuImage.prototype, "format", void 0);
__decorate([
    lit_element_1.property()
], GtcWcMenuImage.prototype, "notation", void 0);
GtcWcMenuImage = __decorate([
    lit_element_1.customElement('gtc-wc-menu-image')
], GtcWcMenuImage);
exports.GtcWcMenuImage = GtcWcMenuImage;
