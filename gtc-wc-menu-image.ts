import {
  LitElement, html, customElement, property
} from 'lit-element';

import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';
import 'web-animations-js/web-animations-next.min.js';

@customElement('gtc-wc-menu-image')
export class GtcWcMenuImage extends LitElement {

  @property()
  styleFormat = "compact";

  @property()
  format = "png";

  @property()
  notation = "cfg";

  render() {
    return html`
<style>
</style>
  <div>

<paper-dropdown-menu label="Style">
  <paper-listbox slot="dropdown-content" selected="${this.styleFormat}" attr-for-selected="value" @iron-select="${this.updateStyle.bind(this)}">
    <paper-item value="compact">compact</paper-item>
    <paper-item value="normal">normal</paper-item>
    <paper-item value="normalinfo">normalinfo</paper-item>
  </paper-listbox>
</paper-dropdown-menu>

<paper-dropdown-menu label="Format" >
<paper-listbox slot="dropdown-content" selected="${this.format}" attr-for-selected="value" @iron-select="${this.updateFormat.bind(this)}">
    <paper-item value="png">png</paper-item>
    <paper-item value="jpg">jpg</paper-item>
    <paper-item value="svg">svg</paper-item>
  </paper-listbox>
</paper-dropdown-menu>

<paper-dropdown-menu label="Notation">
  <paper-listbox slot="dropdown-content" selected="${this.notation}" attr-for-selected="value" @iron-select="${this.updateNotation.bind(this)}">
    <paper-item value="cfg">cfg</paper-item>
    <paper-item value="cfgbw">cfgbw</paper-item>
    <paper-item value="cfg-uoxf">cfg-uoxf</paper-item>
    <paper-item value="uoxf">uoxf</paper-item>
    <paper-item value="uoxf-color">uoxf-color</paper-item>
    <paper-item value="iupac">iupac</paper-item>
  </paper-listbox>
</paper-dropdown-menu>
  </div>
`;
  }

  updateStyle(e)  {
    this.styleFormat = e.detail.item.innerText;
    let event = new CustomEvent('style-update-event', {
      detail: {
        message: 'style changed to' + this.styleFormat,
        value: this.styleFormat
      }
    });
    this.dispatchEvent(event);
  }

  updateFormat(e)  {
    this.format = e.detail.item.innerText;
    let event = new CustomEvent('format-update-event', {
      detail: {
        message: 'format changed to' + this.format,
        value: this.format
      }
    });
    this.dispatchEvent(event);
  }

  updateNotation(e)  {
    this.notation = e.detail.item.innerText;
    let event = new CustomEvent('notation-update-event', {
      detail: {
        message: 'notation changed to' + this.notation,
        value: this.notation
      }
    });
    this.dispatchEvent(event);
  }
}
